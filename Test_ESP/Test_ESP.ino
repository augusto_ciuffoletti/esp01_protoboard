#include <atlib.h>
boolean pup() { true;}
//ESP esp1(9,5,19200,8); // RX,TX,baudrate,CH_PD (CADUTA)
ESP esp1(9,8,19200,10); // RX,TX,baudrate,CH_PD (protoshield)
void setup() {
  Serial.begin(57600);
  esp1.powerUp(10,pup,true);
}
void loop() {
  esp1.atcmd("AT+GMR",10,true);
  esp1.atcmd("AT+CWLAP",10,true);
  esp1.atcmd("AT+PING=\"www.google.com\"",10,true);
//  esp1.atcmd("AT+CWJAP_DEF=\"Fangorn\",\"di qui non passi\"",10,true); // Send command and print response
  delay(2000);
}
